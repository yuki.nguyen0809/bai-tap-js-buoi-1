// Bài 1: Tiền lương nhân viên
// Đầu vào: 
// - Lương 1 ngày: 100.000
// - Số ngày làm: 24 ngày
// Đầu ra:
// - Tổng lương 24 ngày=lương 1n*số ngày làm
var unique = 100000;
var days = 24;
var salary = unique * days;
console.log(``, salary);

// Bài 2: Tính giá trị trung bình
// Đầu vào: 
// - 5 số thực: 14,73,25,59,48
// Đầu ra:
// - Gía trị trung bình 5 số này
var num1 = 14;
var num2 = 73;
var num3 = 25;
var num4 = 59;
var num5 = 48;
var Avgnum = (num1 + num2 + num3 + num4 + num5) / 5;
console.log(``, Avgnum);

// Bài 3: Quy doi tien
// Đầu vào: 
// - USD: 4
// Đầu ra:
// - VND: 23500*USD

var usdEl = document.getElementById("usd");
var vndEl = document.getElementById("vnd");
var vndValue;
function chuyenDoi() {
    vndValue = usdEl.value * 23500;
    vndEl.value = vndValue;
}

// Bài 4: Tinh dien tich, chu vi HCN
// Đầu vào: 
// - CD: 6cm;
// - CR: 4cm;
// Đầu ra:
// - S=CD*CR
// - CV= (CD+CR)*2

var dai = 6;
var rong = 4;
var dienTich = dai * rong;
var chuVi = (dai + rong) * 2;
console.log(``, dienTich);
console.log(``, chuVi);

// Bài 5: Tinh tong 2 ky so
// Đầu vào: 
// - num1:89
// Đầu ra:
// -Ket qua = 17

var num = 89;
var ten = Math.floor(num / 10 % 10);
var unit = num % 10;

var result1 = ten + unit;
console.log(``, result1);
